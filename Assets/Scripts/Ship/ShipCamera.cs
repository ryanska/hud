﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCamera : MonoBehaviour
{
    [SerializeField] private string mouseXAxisName = "Mouse X";
    [SerializeField] private string mouseYAxisName = "Mouse Y";
    [SerializeField] private float mouseSensitivity = 120;
    [SerializeField] private Transform ship;

    private float xAxisClamp;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        xAxisClamp = 0.0f;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis(mouseXAxisName) * mouseSensitivity;
        float mouseY = Input.GetAxis(mouseYAxisName) * mouseSensitivity;

        xAxisClamp += mouseY;

        if (xAxisClamp > 90.0f)
        {
            xAxisClamp = 90.0f;
            mouseY = 0.0f;
            stopRotation(270.0f);
        }
        else if (xAxisClamp < -90.0f)
        {
            xAxisClamp = -90.0f;
            mouseY = 0.0f;
            stopRotation(90.0f);
        }

        transform.Rotate(Vector3.left * mouseY);
        ship.Rotate(Vector3.up * mouseX);
    }

    private void stopRotation(float stopValue)
    {
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = stopValue;
        transform.eulerAngles = eulerRotation;
    }
}
