﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    [SerializeField] private string leftRightName = "Horizontal";
    [SerializeField] private string upDownName = "Vertical";
    [SerializeField] private float speed = 50f;

    public int health = 100;
    public WeaponType weaponType = WeaponType.MISSILE;
    public int ammo = 20000;

    private CharacterController characterController;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        movement();
    }

    private void movement()
    {
        float upDown = Input.GetAxis(upDownName) * speed;
        float leftRight = Input.GetAxis(leftRightName) * speed;

        Vector3 forward = transform.forward * upDown;
        Vector3 side = transform.right * leftRight;

        characterController.SimpleMove(forward + side);
    }

    public void Hit (Bullet bullet)
    {
        health -= bullet.Damage;
    }
}
