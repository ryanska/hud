﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSystem : MonoBehaviour
{
    [SerializeField] List<GameObject> availableBullets;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetBullet(WeaponType weaponType)
    {
        return availableBullets[(int) weaponType];
    }
}
