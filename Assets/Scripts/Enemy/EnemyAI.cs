﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] float speed = 30f;
    [SerializeField] GameObject target;
    [SerializeField] float fowAngle = 120f;
    [SerializeField] WeaponType weaponType;

    private Vector3 lastSeen = Vector3.zero;
    private Quaternion rotationToTarget = Quaternion.identity;
    private bool sawShip = false;
    private Weapon turretWeapon;
    private TurretSystem globalTurretSystem;
    private float fireDelay = 0f;


    SphereCollider distanceOfView;
    GameObject turret;
    GameObject tip;

    // Start is called before the first frame update
    void Start()
    {
        turret = this.gameObject;
        tip = transform.Find("Tip").gameObject;
        globalTurretSystem = this.GetComponentInParent<TurretSystem>();

        distanceOfView = this.transform.GetComponent<SphereCollider>();

        //initialize turret with infinte ammo
        turretWeapon = new Weapon(weaponType, -1);
    }

    // Update is called once per frame
    void Update()
    {
        fireDelay += Time.deltaTime;

        if (sawShip) {
            if (lastSeen !=  target.transform.position) { 
                lastSeen = target.transform.position;
                rotationToTarget = Quaternion.LookRotation(lastSeen - transform.position);
            }

            if (turret.transform.rotation != rotationToTarget)
            {
                turret.transform.rotation = Quaternion.RotateTowards(turret.transform.rotation, rotationToTarget, speed * Time.deltaTime);
            }


            if (fireDelay > turretWeapon.FireRate) {
                fireDelay = 0;
                Bullet bullet = turretWeapon.GetBullet();
                GameObject bulletClone = Instantiate(globalTurretSystem.GetBullet(bullet.Type), tip.transform.position, turret.transform.rotation);
                bulletClone.gameObject.AddComponent<BulletBehaviour>();
                bulletClone.gameObject.GetComponent<BulletBehaviour>().bullet = bullet;
                bulletClone.gameObject.GetComponent<BulletBehaviour>().Fire(tip.transform.forward);


                Destroy(bulletClone.gameObject, 10f);
            }
        }
    }


    void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            sawShip = false;

            Vector3 directionToTarget = target.transform.position - tip.transform.position;
            float angleToTarget = Vector3.Angle(directionToTarget, tip.transform.forward);

            //check if player is within field of view, and cast a ray to make sure he's not obstructed
            if (angleToTarget < fowAngle * 0.5f)
            {
                if (Physics.Raycast(tip.transform.position, directionToTarget, out RaycastHit hit, 300))
                {
                    if (hit.collider.gameObject == target)
                    {
                        sawShip = true;
                        lastSeen = target.transform.position;
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            sawShip = false;
        }
    }
}
