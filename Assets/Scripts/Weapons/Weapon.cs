﻿
using System;
using System.Collections.Generic;
using UnityEngine;

public class Weapon
{
    private WeaponType weaponType;
    private int noOfBullets;
    private float fireRate;

    public Weapon(WeaponType weaponType, int noOfBullets)
    {
        this.weaponType = weaponType;
        this.noOfBullets = noOfBullets;

        switch(weaponType) {
            case (WeaponType.BULLET):
                fireRate = 0.5f;
                break;

            case (WeaponType.MISSILE):
                fireRate = 1f;
                break;

            case (WeaponType.LASER):
                fireRate = 0.05f;
                break;
        }
    }

    public Weapon(WeaponType weaponType)
    {
        this.weaponType = weaponType;
        this.noOfBullets = 0;
    }

    public int AddBullets(int noOfBullets)
    {
        this.noOfBullets += noOfBullets;
        return this.noOfBullets;
    }

    public Bullet GetBullet()
    {
        if (noOfBullets == 0)
        {
            return null;
        }
        else if (noOfBullets == -1)
        {
            ;
        }
        else
        {
            noOfBullets--;
        }


        switch (weaponType)
        {
            case (WeaponType.MISSILE):
                {
                    return new MissileBullet(2, 100);
                }
            case (WeaponType.LASER):
                {
                    return new LazerBullet(1, 200);
                }
            default:
                {
                    return new DefaultBullet(1, 100);
                }
        }
    }

    public static Weapon GetDefaultWeapon()
    {
        return new Weapon(WeaponType.BULLET, -1);
    }

    public WeaponType WeaponType
    {
        get { return this.weaponType; }
        set { this.weaponType = value; }
    }

    public int NoOfBullets
    {
        get { return this.noOfBullets; }
        set { this.noOfBullets = value; }
    }

    public float FireRate
    {
        get { return this.fireRate; }
        set { this.fireRate = value; }
    }

    // implementation:
    public class WeaponSort : IComparer<Weapon>
    {
        public int Compare(Weapon x, Weapon y)
        {
            return (y.WeaponType - x.WeaponType);
        }

    }
}

