﻿using System;
using UnityEngine;

public abstract class Bullet
{
    protected int damage;
    protected float speed;
    protected WeaponType weaponType;

    public Bullet(int damage, float speed, WeaponType weaponType)
    {
        this.damage = damage;
        this.speed = speed;
        this.weaponType = weaponType;
    }

    public int Damage
    {
        get { return this.damage; }
        set { this.damage = value; }
    }

    public float Speed
    {
        get { return this.speed; }
        set { this.speed = value; }
    }

    public WeaponType Type
    {
        get { return this.weaponType; }
        set { this.weaponType = value; }
    }

}

