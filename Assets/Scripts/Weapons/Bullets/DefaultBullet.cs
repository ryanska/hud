﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;


public class DefaultBullet : Bullet
{
    public DefaultBullet(int damage, float speed) 
    : base(damage, speed, WeaponType.BULLET)
    {

    }
}

