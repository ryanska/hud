﻿using System;
using UnityEngine;

public class LazerBullet : Bullet
{
    public LazerBullet(int damage, float speed) 
       : base(damage, speed, WeaponType.LASER)
    {

    }
}
