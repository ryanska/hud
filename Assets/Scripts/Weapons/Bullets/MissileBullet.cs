﻿using System;
using UnityEngine;

public class MissileBullet : Bullet
{
    public MissileBullet(int damage, float speed) 
    : base(damage, speed, WeaponType.MISSILE)
    {

    }

}
