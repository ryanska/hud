﻿
using System;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public Bullet bullet;
    private bool isFired = false;
    private Vector3 direction;

    void Start()
    {
    }
    void Update()
    {
        if (isFired) { 
            transform.position += direction * (bullet.Speed * Time.deltaTime);
        }
    }

    public void Fire(Vector3 origin)
    {
        direction = origin;
        isFired = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<ShipController>().Hit(bullet);

        }

        Destroy(gameObject);

    }
}

