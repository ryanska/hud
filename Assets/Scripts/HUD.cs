﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{

    [SerializeField] GameObject ship;
    [SerializeField] GameObject turrets;
    [SerializeField] GameObject poi;

    private int health;
    private WeaponType weaponType;
    private int ammo;

    private Text healthValue;
    private Text weaponValue;
    private Text ammoValue;

    private ShipController shipController;

    private GameObject mainCamera;
    private List<GameObject> allTurrets = new List<GameObject>();
    private List<GameObject> allPOI = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        healthValue = transform.Find("Health/HealthValue").gameObject.GetComponent<Text> ();
        weaponValue = transform.Find("Weapon/WeaponValue").gameObject.GetComponent<Text> ();
        ammoValue = transform.Find("Ammo/AmmoValue").gameObject.GetComponent<Text> ();

        shipController = ship.GetComponent<ShipController>();
        mainCamera = ship.transform.Find("ShipPov").gameObject;

        int i = 1;

        foreach (Transform child in turrets.transform)
        {
            allTurrets.Add(child.gameObject);

            GameObject turretText = Instantiate(poi, this.transform);
            turretText.SetActive(true);
            turretText.transform.Find("text").gameObject.GetComponent<Text>().text = i.ToString();
            i++;
            allPOI.Add(turretText);
        }
    }
     
    // Update is called once per frame
    void Update()
    {
        healthValue.text = shipController.health.ToString();
        weaponValue.text = shipController.weaponType.ToString();
        if (shipController.ammo == -1)
        {
            ammoValue.text = "Lots";
        }
        else { 
            ammoValue.text = shipController.ammo.ToString();
        }

        for (int i = 0; i < allTurrets.Count; i++)
        {
            Vector3 viewPortPos = mainCamera.GetComponent<Camera>().WorldToViewportPoint(allTurrets[i].transform.position);
            allPOI[i].SetActive(true);

            Vector3 directionToTurret = allTurrets[i].transform.position - mainCamera.transform.position;
            Debug.DrawRay(mainCamera.transform.position, directionToTurret, Color.red);

            if (viewPortPos.x >= 0 && viewPortPos.x <= 1 && viewPortPos.y >= 0 && viewPortPos.y <= 1 && viewPortPos.z > 0)
            {
                //object is in screen ... raycast, and if it can be seen deactivate it.
                if (Physics.Raycast(mainCamera.transform.position, directionToTurret, out RaycastHit hit))
                {
                    if (hit.transform.tag != "Terrain")
                    {
                        //can be seen
                        allPOI[i].SetActive(false);
                    }
                    else
                    {
                        Debug.Log(hit.transform.name);
                    }
                }
            }
            else
            {
                if (viewPortPos.x < 0)
                {
                    viewPortPos.x = 0.01f;
                }
                if (viewPortPos.x > 1)
                {
                    viewPortPos.x = 0.99f;
                }


                if (viewPortPos.y < 0)
                {
                    viewPortPos.y = 0.01f;
                }
                if (viewPortPos.y > 1)
                {
                    viewPortPos.y = 0.99f;
                }


                //if z is negative they're behind - put at bottom of the scree
                if (viewPortPos.z < 0)
                {
                    viewPortPos.y = 0.01f;
                }

            }

            Rect rootRect = this.transform.GetComponent<Canvas>().rootCanvas.pixelRect;
            viewPortPos.x *= rootRect.width;
            viewPortPos.y *= rootRect.height;
            allPOI[i].transform.position = viewPortPos;

        }
    }
}
