TO RUN:
You can open the code in unity and run Assets/Scenes/SampleScene.unity
alternatively import RyanHUD.unitypackage as a package, and run SampleScene.unity

TO PLAY:

The game consists of a terrain, and a hover ship cruising through it.  There
are three turrets
1. Bullets - damage: 1, speed: medium
2. Lazer - damage: 1, speed: high
3. Missiles - damage: 2, speed: low

The turrets will sense the player's ship if it is within 30m and in the Field
of view of 120 degrees.  To escape the turrets just run away from them
or hide behind hills.

The little Point of interest guides will guide you where your enemies are if they're out of your field of view.  To control the ship user arrow keys (forward, back, side left, side right), and the mouse to look all over the area.


TEST:

The following tasks were implemented:
1) Logic in Assets/Scripts/HUD.cs.  The HUD also shows your health, ammo remaining and which weapon you are using.  Changing the weapon wasn't implemented yet.

2) Logic in Assets/Scripts/Ship/ShipController.cs and ShipCamera.cs

3) Logic in Assets/Enemy/EnemyAI.cs .  A turret weapon system was implement, and each turret can shoot either bullets, lasers or missiles, each with different speeds and damage.  These are completely configurable in the game object.

4) Not implemented
5) Not implemented


CREDITS:

No 3rd party scripts were used however the following textures were downloaded from asset store:
https://assetstore.unity.com/packages/2d/textures-materials/floors/terrain-textures-snow-free-samples-54630
